# -*- encoding: utf-8 -*-
"""
@Date       : 2022/02/25
@Author     : xuzhanhong 
@Description: 前端骨架和路由
"""
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

from server import app

from views.index import index_page
from views.table import table_page
from views.upload import upload_page

app.layout = html.Div(
    [
        # 监听url变化
        dcc.Location(id='url'),
        html.Div(
            [
                # 标题区域
                html.Div(
                    html.H3(
                        '数据库管理工具',
                        style={
                            'marginTop': '20px',
                            'fontFamily': 'SimSun',
                            'fontWeight': 'bold'
                        }
                    ),
                    style={
                        'textAlign': 'center',
                        'margin': '0 10px 0 10px',
                        'borderBottom': '2px solid black'
                    }
                ),

                # 子页面区域
                html.Hr(),

                dbc.Nav(
                    [
                        dbc.NavLink('首页', href='/', active="exact"),
                        dbc.NavLink('数据查询', href='/table', active="exact"),
                        dbc.NavLink('数据上传', href='/upload', active="exact"),
                    ],
                    vertical=True,
                    pills=True
                )
            ],
            style={
                'flex': 'none',
                'width': '300px',
                'backgroundColor': '#fafafa'
            }
        ),
        html.Div(
            id='page-content',
            style={
                'flex': 'auto'
            }
        )
    ],
    style={
        'width': '100vw',
        'height': '100vh',
        'display': 'flex'
    }
)


# 路由总控
@app.callback(
    Output('page-content', 'children'),
    Input('url', 'pathname')
)
def render_page_content(pathname):
    if pathname == '/':
        return index_page

    elif pathname == '/table':
        return table_page

    elif pathname == '/upload':
        return upload_page

    return html.H1('您访问的页面不存在！')


if __name__ == '__main__':
    app.run_server(debug=True, port=8888)

