# -*- encoding: utf-8 -*-
"""
@Date       : 2022/03/01
@Author     : xuzhanhong 
@Description: 首页
"""
import dash_html_components as html

from server import app

index_page = html.Div(
    [
        html.P(
            [
                html.Span('欢迎来到基于'),
                html.Strong('dash', style={'color': 'rgb(13, 103, 180)'}),
                html.Em(html.Sup('[1]', style={'fontSize': '16px'})),
                html.Span('开发的'),
                html.Strong('数据库管理工具'),
                html.Em(html.Sup('[2]', style={'fontSize': '16px'})),
                html.Span('，'),
                html.Span('作者'),
                html.A('xuzhanhong', href='https://gitee.com/xuzhanhong/database-management-tool', target='_blank'),
                html.Span('。')
            ],
            id='index-desc',
            style={
                'margin': '50px 0 50px 50px'
            }
        ),
        html.Div(
            [
                html.Div(
                    [
                        html.Span('微信公众号【活用数据】：', style={'fontWeight': 'bold', 'fontSize': '10px'}),
                        html.Img(src=app.get_asset_url('img/hysj.jpg'), style={'width': '100%'})
                    ],
                    style={
                        'flex': 'none',
                        'width': '250px',
                        'marginLeft': '75px'
                    }
                )
            ],
            style={
                'display': 'flex',
                'justifyContent': 'center',
                'borderTop': '1px solid #e0e0e0',
                'borderBottom': '1px solid #e0e0e0',
                'margin': '0 50px 0 50px'
            }
        ),
        html.P(
            [
                html.Em('[1] '),
                html.Span('一个面向数据分析相关人员，纯Python即可实现交互式web应用，极大程度上简化web应用开发难度的先进框架。'),
                html.Br(),
                html.Br(),
                html.Em('[2] '),
                html.Span('具备简单的数据库查询，筛选，下载，数据上传等功能，开发过程参考教程：'),
                html.A('https://www.cnblogs.com/feffery/tag/Dash/',
                       href='https://www.cnblogs.com/feffery/tag/Dash/',
                       target='_blank'),
                html.Span('。'),
            ],
            style={
                'margin': '50px 0 0 50px'
            }
        )
    ]
)
