# DatabaseManagementTools

#### 介绍

> 参考资料：[https://www.cnblogs.com/feffery/tag/Dash/](https://www.cnblogs.com/feffery/tag/Dash/)

使用Python的Dash框架开发的数据库管理工具。



功能：
* 数据库数据查询
* 查询结果二次过滤
* 下载查询结果
* 修改数据库内容
* 上传csv文件到数据库



#### 网站截图

##### 首页

<img src="https://tva1.sinaimg.cn/large/e6c9d24egy1gzug1qsoi3j21uq0u0tc6.jpg">



##### 数据查询

<img src="https://tva1.sinaimg.cn/large/e6c9d24egy1gzug3jsrubj21vc0u0tb3.jpg">



##### 数据上传

<img src="https://tva1.sinaimg.cn/large/e6c9d24egy1gzug4414hej21vz0u0mze.jpg">



#### 软件架构

软件架构说明

```
.
├── README.md
├── app.py
├── assets
│   ├── css
│   │   ├── bootstrap.min.css
│   │   └── custom.css
│   ├── favicon.ico
│   └── img
│       └── hysj.jpg
├── config
│   ├── __init__.py
│   └── config.py
├── downloads
├── requirements.txt
├── server.py
├── upload
└── views
    ├── __init__.py
    ├── index.py
    ├── table.py
    └── upload.py
```



#### 安装教程

1. 创建并激活虚拟环境

   ```
   conda create -n dash-dev python=3.7 -y
   conda activate dash-dev
   ```

2. 安装相应的包

   ```
   pip install -r requirements.txt
   ```

3. 运行服务

   ```
   python app.py
   ```

   

#### 其他说明

使用 `pip` 生成 `requirements.txt` 文件：

```
pip freeze > requirements.txt
```



