from email import header


# -*- encoding: utf-8 -*-
"""
@Date       : 2022/03/01
@Author     : xuzhanhong 
@Description: 配置
"""
global_config = {
    # 数据库连接配置
    "db": {
        "url": "mysql+pymysql://root:12345678@localhost:3306",
        "base_schema": "pg_tables"
    }
}